package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbSysConfig
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbSysConfigRepository : JpaRepository<MbSysConfig, Long>,
    QueryDslPredicateExecutor<MbSysConfig> {
    fun findByCfgName(cfgName: String): MbSysConfig
}

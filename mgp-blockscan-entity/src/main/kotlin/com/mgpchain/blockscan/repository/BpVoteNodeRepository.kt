package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.BpVoteNode
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface BpVoteNodeRepository : JpaRepository<BpVoteNode, Long>,
    QueryDslPredicateExecutor<BpVoteNode>

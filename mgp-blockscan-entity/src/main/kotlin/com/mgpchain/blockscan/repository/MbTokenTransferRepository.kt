package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbTokenTransfer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbTokenTransferRepository : JpaRepository<MbTokenTransfer, Long>,
    QueryDslPredicateExecutor<MbTokenTransfer>

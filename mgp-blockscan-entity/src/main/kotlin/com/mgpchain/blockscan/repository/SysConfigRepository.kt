package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.SysConfig
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface SysConfigRepository : JpaRepository<SysConfig, Long>,
    QueryDslPredicateExecutor<SysConfig> {
    fun findByName(name: String): String

    @Query(value = "select * from sys_config where name=?", nativeQuery = true)
    fun findSysConfigByName(cfgName: String): SysConfig?
}

package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbBlock
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbBlockRepository : JpaRepository<MbBlock, Long>,
    QueryDslPredicateExecutor<MbBlock> {
    fun findFirstByOrderByBlockNumDesc(): MbBlock?
}

package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbActions
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbActionsRepository : JpaRepository<MbActions, Long>,
    QueryDslPredicateExecutor<MbActions>

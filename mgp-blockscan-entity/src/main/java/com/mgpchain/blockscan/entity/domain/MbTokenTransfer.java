package com.mgpchain.blockscan.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * MbTokenTransfer is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class MbTokenTransfer implements Serializable {

    @Column("action")
    private String action;

    @Column("amount")
    private java.math.BigDecimal amount;

    @Column("contract")
    private String contract;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("from_account")
    private String fromAccount;

    @Column("height")
    private Long height;

    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    @Column("id")
    private Long id;

    @Column("memo")
    private String memo;

    @Column("symbol")
    private String symbol;

    @Column("to_account")
    private String toAccount;

    @Column("txid")
    private String txid;

    @Column("txtime")
    private java.util.Date txtime;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public java.math.BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public java.util.Date getTxtime() {
        return txtime;
    }

    public void setTxtime(java.util.Date txtime) {
        this.txtime = txtime;
    }

}


package com.mgpchain.blockscan.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * BpVoteNode is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class BpVoteNode implements Serializable {

    @Column("create_at")
    private java.util.Date createAt;

    @Column("hash")
    private String hash;

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column("id")
    private Long id;

    @Column("mgp_address")
    private String mgpAddress;

    @Column("node_content")
    private String nodeContent;

    @Column("node_head_img")
    private String nodeHeadImg;

    @Column("node_id")
    private Integer nodeId;

    @Column("node_name")
    private String nodeName;

    @Column("node_reward_rule")
    private String nodeRewardRule;

    @Column("node_share_ratio")
    private Integer nodeShareRatio;

    @Column("node_url")
    private String nodeUrl;

    @Column("update_at")
    private java.util.Date updateAt;

    public java.util.Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(java.util.Date createAt) {
        this.createAt = createAt;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMgpAddress() {
        return mgpAddress;
    }

    public void setMgpAddress(String mgpAddress) {
        this.mgpAddress = mgpAddress;
    }

    public String getNodeContent() {
        return nodeContent;
    }

    public void setNodeContent(String nodeContent) {
        this.nodeContent = nodeContent;
    }

    public String getNodeHeadImg() {
        return nodeHeadImg;
    }

    public void setNodeHeadImg(String nodeHeadImg) {
        this.nodeHeadImg = nodeHeadImg;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeRewardRule() {
        return nodeRewardRule;
    }

    public void setNodeRewardRule(String nodeRewardRule) {
        this.nodeRewardRule = nodeRewardRule;
    }

    public Integer getNodeShareRatio() {
        return nodeShareRatio;
    }

    public void setNodeShareRatio(Integer nodeShareRatio) {
        this.nodeShareRatio = nodeShareRatio;
    }

    public String getNodeUrl() {
        return nodeUrl;
    }

    public void setNodeUrl(String nodeUrl) {
        this.nodeUrl = nodeUrl;
    }

    public java.util.Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(java.util.Date updateAt) {
        this.updateAt = updateAt;
    }

}


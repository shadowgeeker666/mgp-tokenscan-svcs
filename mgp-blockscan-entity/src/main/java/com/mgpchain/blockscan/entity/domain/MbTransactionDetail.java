package com.mgpchain.blockscan.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * MbTransactionDetail is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class MbTransactionDetail implements Serializable {

    @Column("account")
    private String account;

    @Column("amount")
    private java.math.BigDecimal amount;

    @Column("block_num")
    private Long blockNum;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("from_account")
    private String fromAccount;

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column("id")
    private Long id;

    @Column("memo")
    private String memo;

    @Column("quantity")
    private String quantity;

    @Column("symbol")
    private String symbol;

    @Column("to_account")
    private String toAccount;

    @Column("transaction_id")
    private String transactionId;

    @Column("tx_name")
    private String txName;

    @Column("tx_time")
    private java.util.Date txTime;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public java.math.BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }

    public Long getBlockNum() {
        return blockNum;
    }

    public void setBlockNum(Long blockNum) {
        this.blockNum = blockNum;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTxName() {
        return txName;
    }

    public void setTxName(String txName) {
        this.txName = txName;
    }

    public java.util.Date getTxTime() {
        return txTime;
    }

    public void setTxTime(java.util.Date txTime) {
        this.txTime = txTime;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}


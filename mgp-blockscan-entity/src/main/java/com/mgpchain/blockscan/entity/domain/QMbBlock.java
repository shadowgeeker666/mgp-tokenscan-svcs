package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbBlock is a Querydsl query type for MbBlock
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbBlock extends com.querydsl.sql.RelationalPathBase<MbBlock> {

    private static final long serialVersionUID = 216752639;

    public static final QMbBlock mbBlock = new QMbBlock("mb_block");

    public final StringPath actionMroot = createString("actionMroot");

    public final StringPath blockExtensions = createString("blockExtensions");

    public final StringPath blockId = createString("blockId");

    public final NumberPath<Long> blockNum = createNumber("blockNum", Long.class);

    public final NumberPath<Integer> confirmed = createNumber("confirmed", Integer.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath headerExtensions = createString("headerExtensions");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath newProducers = createString("newProducers");

    public final StringPath previous = createString("previous");

    public final StringPath producer = createString("producer");

    public final StringPath producerSignature = createString("producerSignature");

    public final NumberPath<Long> refBlockPrefix = createNumber("refBlockPrefix", Long.class);

    public final NumberPath<Integer> scheduleVersion = createNumber("scheduleVersion", Integer.class);

    public final DateTimePath<java.util.Date> timestamp = createDateTime("timestamp", java.util.Date.class);

    public final StringPath transactionMroot = createString("transactionMroot");

    public final StringPath transactions = createString("transactions");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbBlock> primary = createPrimaryKey(id);

    public QMbBlock(String variable) {
        super(MbBlock.class, forVariable(variable), "null", "mb_block");
        addMetadata();
    }

    public QMbBlock(String variable, String schema, String table) {
        super(MbBlock.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbBlock(String variable, String schema) {
        super(MbBlock.class, forVariable(variable), schema, "mb_block");
        addMetadata();
    }

    public QMbBlock(Path<? extends MbBlock> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_block");
        addMetadata();
    }

    public QMbBlock(PathMetadata metadata) {
        super(MbBlock.class, metadata, "null", "mb_block");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(actionMroot, ColumnMetadata.named("action_mroot").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(blockExtensions, ColumnMetadata.named("block_extensions").withIndex(13).ofType(Types.VARCHAR).withSize(255));
        addMetadata(blockId, ColumnMetadata.named("block_id").withIndex(14).ofType(Types.VARCHAR).withSize(255));
        addMetadata(blockNum, ColumnMetadata.named("block_num").withIndex(15).ofType(Types.BIGINT).withSize(19));
        addMetadata(confirmed, ColumnMetadata.named("confirmed").withIndex(4).ofType(Types.INTEGER).withSize(10));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(17).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(headerExtensions, ColumnMetadata.named("header_extensions").withIndex(10).ofType(Types.LONGVARCHAR).withSize(2147483647));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(newProducers, ColumnMetadata.named("new_producers").withIndex(9).ofType(Types.LONGVARCHAR).withSize(2147483647));
        addMetadata(previous, ColumnMetadata.named("previous").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(producer, ColumnMetadata.named("producer").withIndex(7).ofType(Types.VARCHAR).withSize(255));
        addMetadata(producerSignature, ColumnMetadata.named("producer_signature").withIndex(11).ofType(Types.VARCHAR).withSize(255));
        addMetadata(refBlockPrefix, ColumnMetadata.named("ref_block_prefix").withIndex(16).ofType(Types.BIGINT).withSize(19));
        addMetadata(scheduleVersion, ColumnMetadata.named("schedule_version").withIndex(8).ofType(Types.INTEGER).withSize(10));
        addMetadata(timestamp, ColumnMetadata.named("timestamp").withIndex(3).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(transactionMroot, ColumnMetadata.named("transaction_mroot").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(transactions, ColumnMetadata.named("transactions").withIndex(12).ofType(Types.LONGVARCHAR).withSize(2147483647));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(18).ofType(Types.TIMESTAMP).withSize(19));
    }

}


package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbBlockTransaction is a Querydsl query type for MbBlockTransaction
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbBlockTransaction extends com.querydsl.sql.RelationalPathBase<MbBlockTransaction> {

    private static final long serialVersionUID = 517747551;

    public static final QMbBlockTransaction mbBlockTransaction = new QMbBlockTransaction("mb_block_transaction");

    public final NumberPath<Long> blockNum = createNumber("blockNum", Long.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath status = createString("status");

    public final StringPath transactionId = createString("transactionId");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbBlockTransaction> primary = createPrimaryKey(id);

    public QMbBlockTransaction(String variable) {
        super(MbBlockTransaction.class, forVariable(variable), "null", "mb_block_transaction");
        addMetadata();
    }

    public QMbBlockTransaction(String variable, String schema, String table) {
        super(MbBlockTransaction.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbBlockTransaction(String variable, String schema) {
        super(MbBlockTransaction.class, forVariable(variable), schema, "mb_block_transaction");
        addMetadata();
    }

    public QMbBlockTransaction(Path<? extends MbBlockTransaction> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_block_transaction");
        addMetadata();
    }

    public QMbBlockTransaction(PathMetadata metadata) {
        super(MbBlockTransaction.class, metadata, "null", "mb_block_transaction");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(blockNum, ColumnMetadata.named("block_num").withIndex(2).ofType(Types.BIGINT).withSize(19));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(5).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(status, ColumnMetadata.named("status").withIndex(4).ofType(Types.VARCHAR).withSize(255));
        addMetadata(transactionId, ColumnMetadata.named("transaction_id").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(6).ofType(Types.TIMESTAMP).withSize(19));
    }

}


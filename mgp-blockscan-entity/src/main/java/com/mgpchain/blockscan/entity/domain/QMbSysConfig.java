package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbSysConfig is a Querydsl query type for MbSysConfig
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbSysConfig extends com.querydsl.sql.RelationalPathBase<MbSysConfig> {

    private static final long serialVersionUID = -1061112607;

    public static final QMbSysConfig mbSysConfig = new QMbSysConfig("mb_sys_config");

    public final StringPath active = createString("active");

    public final StringPath cfgName = createString("cfgName");

    public final StringPath cfgValue = createString("cfgValue");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath module = createString("module");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbSysConfig> primary = createPrimaryKey(id);

    public QMbSysConfig(String variable) {
        super(MbSysConfig.class, forVariable(variable), "null", "mb_sys_config");
        addMetadata();
    }

    public QMbSysConfig(String variable, String schema, String table) {
        super(MbSysConfig.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbSysConfig(String variable, String schema) {
        super(MbSysConfig.class, forVariable(variable), schema, "mb_sys_config");
        addMetadata();
    }

    public QMbSysConfig(Path<? extends MbSysConfig> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_sys_config");
        addMetadata();
    }

    public QMbSysConfig(PathMetadata metadata) {
        super(MbSysConfig.class, metadata, "null", "mb_sys_config");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(active, ColumnMetadata.named("active").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(cfgName, ColumnMetadata.named("cfg_name").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(cfgValue, ColumnMetadata.named("cfg_value").withIndex(4).ofType(Types.VARCHAR).withSize(255));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(7).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(description, ColumnMetadata.named("description").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(module, ColumnMetadata.named("module").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(8).ofType(Types.TIMESTAMP).withSize(19));
    }

}


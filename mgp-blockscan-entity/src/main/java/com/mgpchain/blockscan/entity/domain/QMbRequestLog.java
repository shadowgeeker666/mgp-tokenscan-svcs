package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbRequestLog is a Querydsl query type for MbRequestLog
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbRequestLog extends com.querydsl.sql.RelationalPathBase<MbRequestLog> {

    private static final long serialVersionUID = 1109718019;

    public static final QMbRequestLog mbRequestLog = new QMbRequestLog("mb_request_log");

    public final StringPath accountName = createString("accountName");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath deviceUuid = createString("deviceUuid");

    public final NumberPath<Integer> errorCode = createNumber("errorCode", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath method = createString("method");

    public final StringPath requestIp = createString("requestIp");

    public final StringPath requestParams = createString("requestParams");

    public final StringPath requestUri = createString("requestUri");

    public final StringPath requestUrl = createString("requestUrl");

    public final StringPath requestUuid = createString("requestUuid");

    public final StringPath response = createString("response");

    public final NumberPath<Long> responseTime = createNumber("responseTime", Long.class);

    public final StringPath stackTrace = createString("stackTrace");

    public final NumberPath<Long> timestamp = createNumber("timestamp", Long.class);

    public final com.querydsl.sql.PrimaryKey<MbRequestLog> primary = createPrimaryKey(id);

    public QMbRequestLog(String variable) {
        super(MbRequestLog.class, forVariable(variable), "null", "mb_request_log");
        addMetadata();
    }

    public QMbRequestLog(String variable, String schema, String table) {
        super(MbRequestLog.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbRequestLog(String variable, String schema) {
        super(MbRequestLog.class, forVariable(variable), schema, "mb_request_log");
        addMetadata();
    }

    public QMbRequestLog(Path<? extends MbRequestLog> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_request_log");
        addMetadata();
    }

    public QMbRequestLog(PathMetadata metadata) {
        super(MbRequestLog.class, metadata, "null", "mb_request_log");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(accountName, ColumnMetadata.named("account_name").withIndex(2).ofType(Types.VARCHAR).withSize(256));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(15).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(deviceUuid, ColumnMetadata.named("device_uuid").withIndex(3).ofType(Types.VARCHAR).withSize(64));
        addMetadata(errorCode, ColumnMetadata.named("error_code").withIndex(4).ofType(Types.INTEGER).withSize(10));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(method, ColumnMetadata.named("method").withIndex(6).ofType(Types.VARCHAR).withSize(11));
        addMetadata(requestIp, ColumnMetadata.named("request_ip").withIndex(7).ofType(Types.VARCHAR).withSize(64));
        addMetadata(requestParams, ColumnMetadata.named("request_params").withIndex(8).ofType(Types.LONGVARCHAR).withSize(65535));
        addMetadata(requestUri, ColumnMetadata.named("request_uri").withIndex(9).ofType(Types.VARCHAR).withSize(256));
        addMetadata(requestUrl, ColumnMetadata.named("request_url").withIndex(11).ofType(Types.VARCHAR).withSize(512));
        addMetadata(requestUuid, ColumnMetadata.named("request_uuid").withIndex(5).ofType(Types.VARCHAR).withSize(32));
        addMetadata(response, ColumnMetadata.named("response").withIndex(10).ofType(Types.LONGVARCHAR).withSize(2147483647));
        addMetadata(responseTime, ColumnMetadata.named("response_time").withIndex(12).ofType(Types.BIGINT).withSize(19));
        addMetadata(stackTrace, ColumnMetadata.named("stack_trace").withIndex(13).ofType(Types.LONGVARCHAR).withSize(65535));
        addMetadata(timestamp, ColumnMetadata.named("timestamp").withIndex(14).ofType(Types.BIGINT).withSize(19));
    }

}


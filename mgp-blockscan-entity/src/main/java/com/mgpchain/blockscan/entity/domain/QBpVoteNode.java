package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBpVoteNode is a Querydsl query type for BpVoteNode
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QBpVoteNode extends com.querydsl.sql.RelationalPathBase<BpVoteNode> {

    private static final long serialVersionUID = -717604077;

    public static final QBpVoteNode bpVoteNode = new QBpVoteNode("bp_vote_node");

    public final DateTimePath<java.util.Date> createAt = createDateTime("createAt", java.util.Date.class);

    public final StringPath hash = createString("hash");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath mgpAddress = createString("mgpAddress");

    public final StringPath nodeContent = createString("nodeContent");

    public final StringPath nodeHeadImg = createString("nodeHeadImg");

    public final NumberPath<Integer> nodeId = createNumber("nodeId", Integer.class);

    public final StringPath nodeName = createString("nodeName");

    public final StringPath nodeRewardRule = createString("nodeRewardRule");

    public final NumberPath<Integer> nodeShareRatio = createNumber("nodeShareRatio", Integer.class);

    public final StringPath nodeUrl = createString("nodeUrl");

    public final DateTimePath<java.util.Date> updateAt = createDateTime("updateAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<BpVoteNode> primary = createPrimaryKey(id);

    public QBpVoteNode(String variable) {
        super(BpVoteNode.class, forVariable(variable), "null", "bp_vote_node");
        addMetadata();
    }

    public QBpVoteNode(String variable, String schema, String table) {
        super(BpVoteNode.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBpVoteNode(String variable, String schema) {
        super(BpVoteNode.class, forVariable(variable), schema, "bp_vote_node");
        addMetadata();
    }

    public QBpVoteNode(Path<? extends BpVoteNode> path) {
        super(path.getType(), path.getMetadata(), "null", "bp_vote_node");
        addMetadata();
    }

    public QBpVoteNode(PathMetadata metadata) {
        super(BpVoteNode.class, metadata, "null", "bp_vote_node");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(createAt, ColumnMetadata.named("create_at").withIndex(8).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(hash, ColumnMetadata.named("hash").withIndex(12).ofType(Types.VARCHAR).withSize(255).notNull());
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(mgpAddress, ColumnMetadata.named("mgp_address").withIndex(2).ofType(Types.VARCHAR).withSize(32).notNull());
        addMetadata(nodeContent, ColumnMetadata.named("node_content").withIndex(6).ofType(Types.LONGVARCHAR).withSize(65535));
        addMetadata(nodeHeadImg, ColumnMetadata.named("node_head_img").withIndex(7).ofType(Types.VARCHAR).withSize(128));
        addMetadata(nodeId, ColumnMetadata.named("node_id").withIndex(10).ofType(Types.INTEGER).withSize(10));
        addMetadata(nodeName, ColumnMetadata.named("node_name").withIndex(3).ofType(Types.VARCHAR).withSize(128).notNull());
        addMetadata(nodeRewardRule, ColumnMetadata.named("node_reward_rule").withIndex(5).ofType(Types.LONGVARCHAR).withSize(65535));
        addMetadata(nodeShareRatio, ColumnMetadata.named("node_share_ratio").withIndex(11).ofType(Types.INTEGER).withSize(10));
        addMetadata(nodeUrl, ColumnMetadata.named("node_url").withIndex(4).ofType(Types.VARCHAR).withSize(128));
        addMetadata(updateAt, ColumnMetadata.named("update_at").withIndex(9).ofType(Types.TIMESTAMP).withSize(19));
    }

}


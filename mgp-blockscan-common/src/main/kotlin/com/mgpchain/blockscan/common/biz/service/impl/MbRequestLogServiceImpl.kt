package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbRequestLogService
import com.mgpchain.blockscan.entity.domain.MbRequestLog
import com.mgpchain.blockscan.repository.MbRequestLogRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class MbRequestLogServiceImpl: MbRequestLogService {

    override fun getById(id:Long): MbRequestLog? {
        return mbRequestLogRepository.findOne(id)
    }

    override fun save(mbRequestLog:MbRequestLog): MbRequestLog {
        return mbRequestLogRepository.saveAndFlush(mbRequestLog)
    }

    @Autowired lateinit var mbRequestLogRepository: MbRequestLogRepository
}

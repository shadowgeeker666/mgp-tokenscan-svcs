package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbSysConfigService
import com.mgpchain.blockscan.entity.domain.MbSysConfig
import com.mgpchain.blockscan.repository.MbSysConfigRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class MbSysConfigServiceImpl: MbSysConfigService {

    override fun getById(id:Long): MbSysConfig? {
        return mbSysConfigRepository.findOne(id)
    }

    override fun save(mbSysConfig:MbSysConfig): MbSysConfig {
        return mbSysConfigRepository.saveAndFlush(mbSysConfig)
    }

    override fun getByName(cfgName: String): MbSysConfig {
        return mbSysConfigRepository.findByCfgName(cfgName)
    }

    @Autowired lateinit var mbSysConfigRepository: MbSysConfigRepository
}

package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbBlockService
import com.mgpchain.blockscan.entity.domain.MbBlock
import com.mgpchain.blockscan.repository.MbBlockRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MbBlockServiceImpl: MbBlockService {

    override fun getById(id: Long): MbBlock {
        return mbBlockRepository.findOne(id)
    }

    override fun save(mbBlock: MbBlock): MbBlock {
        return mbBlockRepository.saveAndFlush(mbBlock)
    }

    override fun queryLatestBlock(): MbBlock? {
        return mbBlockRepository.findFirstByOrderByBlockNumDesc()
    }

    @Autowired lateinit var mbBlockRepository: MbBlockRepository
}

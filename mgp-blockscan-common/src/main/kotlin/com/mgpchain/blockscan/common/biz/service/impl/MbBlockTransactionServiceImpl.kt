package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbBlockTransactionService
import com.mgpchain.blockscan.entity.domain.MbBlockTransaction
import com.mgpchain.blockscan.repository.MbBlockTransactionRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort


@Service
class MbBlockTransactionServiceImpl :
    MbBlockTransactionService {

    override fun getById(id: Long): MbBlockTransaction? {
        return mbBlockTransactionRepository.findOne(id)
    }

    override fun save(mbBlockTransaction: MbBlockTransaction): MbBlockTransaction {
        return mbBlockTransactionRepository.saveAndFlush(mbBlockTransaction)
    }

    override fun findMaxBlockNum(): Long {
        val qSort = Sort(Sort.Order(Sort.Direction.DESC, "id"))
        return mbBlockTransactionRepository.findAll(qSort).firstOrNull()?.blockNum ?: 0L
    }

    @Autowired
    lateinit var mbBlockTransactionRepository: MbBlockTransactionRepository
}

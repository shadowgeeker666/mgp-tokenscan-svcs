package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbAccountService
import com.mgpchain.blockscan.entity.domain.MbAccount
import com.mgpchain.blockscan.repository.MbAccountRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal

@Service
class MbAccountServiceImpl: MbAccountService {

    override fun getById(id:Long): MbAccount? {
        return mbAccountRepository.findOne(id)
    }

    override fun save(mbAccount:MbAccount): MbAccount {
        return mbAccountRepository.saveAndFlush(mbAccount)
    }

    override fun genAccount(name: String, blockNum: Long): MbAccount {
        val mbAccount = MbAccount()
        mbAccount.account = name
        mbAccount.createdBlockNum = blockNum
        mbAccount.totalBalance = BigDecimal.ZERO
        mbAccount.staked = BigDecimal.ZERO
        mbAccount.unstaked = BigDecimal.ZERO
        mbAccount.syncFlag = 0
        return mbAccountRepository.save(mbAccount)
    }

    override fun addSyncFlag(vararg name: String) {
        name.forEach {
            //每次有交易过后，同步三次余额数据
            val account = mbAccountRepository.findByAccount(it) ?: genAccount(name = it, blockNum = 0)
            account.syncFlag = 3
            mbAccountRepository.save(account)
        }
    }

    override fun findBySyncFlagGreaterThan(i: Int): List<MbAccount>? {
        return mbAccountRepository.findBySyncFlagGreaterThan(i)
    }

    override fun getByName(name: String): MbAccount? {
        return mbAccountRepository.findByAccount(name)
    }

    @Autowired lateinit var mbAccountRepository: MbAccountRepository
}

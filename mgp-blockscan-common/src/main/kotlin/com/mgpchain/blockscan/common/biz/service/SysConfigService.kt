package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.SysConfig

interface SysConfigService {
    fun getById(id:Long): SysConfig?

    fun save(sysConfig:SysConfig): SysConfig
    fun findByCfgName(name: String): String
}

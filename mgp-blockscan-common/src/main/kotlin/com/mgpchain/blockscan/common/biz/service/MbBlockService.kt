package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbBlock


interface MbBlockService {
    fun getById(id:Long): MbBlock?

    fun save(mbBlock: MbBlock): MbBlock

    fun queryLatestBlock(): MbBlock?
}

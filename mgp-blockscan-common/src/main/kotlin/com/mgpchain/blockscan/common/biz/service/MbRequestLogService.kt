package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbRequestLog

interface MbRequestLogService {
    fun getById(id:Long): MbRequestLog?

    fun save(mbRequestLog:MbRequestLog): MbRequestLog
}

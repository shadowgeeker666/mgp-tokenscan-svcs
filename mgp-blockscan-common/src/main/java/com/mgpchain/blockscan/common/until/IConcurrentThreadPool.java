package com.mgpchain.blockscan.common.until;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author : CHEN
 * @Date : Created in 2021/7/2
 * @description：初始化线程池 submit：提交单个任务的线程，并有返回值，invokeAll：提交多个任务的线程，并有返回值
 * @modified By：
 **/

public interface IConcurrentThreadPool {

    /**
     * 初始化线程池
     */

    void initConcurrentThreadPool(Integer coreSize);

    /**
     * 提交单个任务
     *
     * @param <V>
     * @param task
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */

    <V> V submit(CallableTemplate<V> task) throws InterruptedException, ExecutionException;

    /**
     * 提交多个任务
     *
     * @param <V>
     * @param tasks
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */

    <V> List<V> invokeAll(List<? extends CallableTemplate<V>> tasks) throws InterruptedException, ExecutionException;

}
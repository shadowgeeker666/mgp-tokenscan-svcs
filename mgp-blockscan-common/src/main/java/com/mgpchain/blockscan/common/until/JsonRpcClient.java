package com.mgpchain.blockscan.common.until;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class JsonRpcClient {

    @Setter
    private ObjectMapper mapper;
    @Setter
    private String jsonRpcUrl;
    private Boolean isJsonFormat = true;
    private Logger log = LoggerFactory.getLogger(this.getClass());


    private CloseableHttpClient httpClient;

    public JsonRpcClient(String jsonRpcUrl,
                         Boolean isJsonFormat) {
        super();
        this.jsonRpcUrl = jsonRpcUrl;
        this.mapper = new ObjectMapper();
        this.isJsonFormat = isJsonFormat;
        this.httpClient = HttpClients.createDefault();

    }

    public <U> U executeJsonGet(Class<U> classOut) throws IOException {
        return executeJsonGet(new HashMap<>(), classOut);
    }

    public <U> U executeJsonGet(HashMap<String, String> header, Class<U> classOut) throws IOException {
        HttpGet get = new HttpGet(jsonRpcUrl);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(5000)
                .setConnectionRequestTimeout(5000)
                .setSocketTimeout(5000)
                .setRedirectsEnabled(true)
                .build();
        get.setConfig(requestConfig);
        if (this.isJsonFormat) {
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");
        }

        header.entrySet().forEach(it -> {
            get.setHeader(it.getKey(), it.getValue());
        });

        CloseableHttpResponse response = httpClient.execute(get);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder sb = new StringBuilder();

        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }

        log.info("executeJson:" + sb.toString());
        System.out.println("executeJson:" + sb.toString());

        return JSON.parseObject(sb.toString(), classOut);
    }

    public <U> U executeJsonPost(Object dataIn, Class<U> classOut) throws IOException {
        return executeJsonPost(new HashMap<>(), dataIn, classOut);
    }

    public <U> U executeJsonPost(HashMap<String, String> header, Object dataIn, Class<U> classOut) throws IOException {
        String json = this.mapper.writeValueAsString(dataIn);
        HttpPost post = new HttpPost(jsonRpcUrl);
        StringEntity body = new StringEntity(json, "UTF-8");
        post.setEntity(body);
        if (this.isJsonFormat) {
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json;charset=utf-8;");
        }

        header.entrySet().forEach(it -> {
            post.setHeader(it.getKey(), it.getValue());
        });

        CloseableHttpResponse response = httpClient.execute(post);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder sb = new StringBuilder();

        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }

        log.info("executeJson:" + sb.toString());

        return JSON.parseObject(sb.toString(), classOut);
    }
}

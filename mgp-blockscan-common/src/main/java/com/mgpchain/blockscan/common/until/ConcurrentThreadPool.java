package com.mgpchain.blockscan.common.until;

import org.springframework.scheduling.concurrent.DefaultManagedAwareThreadFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author : CHEN
 * @Date : Created in 2021/7/2
 * @modified By：
 **/
public class ConcurrentThreadPool implements IConcurrentThreadPool {

    private ThreadPoolExecutor threadPoolExecutor;

    @Override
    public void initConcurrentThreadPool(Integer coreSize) {
        // 超时时间30秒
        long keepAliveTime = 30;
        // 核心线程数
        int corePoolSize = coreSize;
        // 最大线程数
        int maximumPoolSize = 30;
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(2000),
                new DefaultManagedAwareThreadFactory(),
                new ThreadPoolExecutor.DiscardOldestPolicy()
        );
    }

    @Override
    public <V> List<V> invokeAll(List<? extends CallableTemplate<V>> tasks) throws InterruptedException, ExecutionException {
        List<Future<V>> tasksResult = threadPoolExecutor.invokeAll(tasks);
        List<V> resultList = new ArrayList<V>();
        for (Future<V> future : tasksResult) {
            resultList.add(future.get());
        }
        return resultList;
    }

    @Override
    public <V> V submit(CallableTemplate<V> task) throws InterruptedException, ExecutionException {
        Future<V> result = threadPoolExecutor.submit(task);
        return result.get();
    }

}

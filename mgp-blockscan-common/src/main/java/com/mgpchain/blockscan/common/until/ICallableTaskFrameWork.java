package com.mgpchain.blockscan.common.until;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author : CHEN
 * @Date : Created in 2021/7/2
 * @modified By：
 **/
public interface ICallableTaskFrameWork {

    <V> List<V> submitsAll(List<? extends CallableTemplate<V>> tasks)

            throws InterruptedException, ExecutionException;

}

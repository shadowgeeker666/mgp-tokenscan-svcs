package com.mgpchain.tokenscan.config

import com.alibaba.druid.pool.DruidDataSource
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import java.sql.SQLException
import javax.sql.DataSource

@Configuration
open class DataSourceCfg {
    private val logger = LoggerFactory.getLogger(javaClass)

    @Bean
    @Primary
    open fun dataSource(): DataSource {
        val datasource = DruidDataSource()

        datasource.url = Environment.MYSQL_URL
        datasource.username = Environment.MYSQL_USERNAME
        datasource.password = Environment.MYSQL_PASSWORD
        datasource.driverClassName = Environment.MYSQL_DRIVER

        //configuration
        datasource.initialSize = 20
        datasource.minIdle = 10
        datasource.maxActive = 40
        datasource.maxWait = 5000
        datasource.timeBetweenEvictionRunsMillis = 10000
        datasource.minEvictableIdleTimeMillis = 30000
        datasource.isTestWhileIdle = true
        datasource.isTestOnBorrow = false
        datasource.isTestOnReturn = false
        datasource.isPoolPreparedStatements = true
        datasource.maxPoolPreparedStatementPerConnectionSize = 20
        datasource.setConnectionProperties("druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000")
        datasource.validationQuery = "select now()"

        try {
            datasource.setFilters("stat,wall,log4j")
        } catch (e: SQLException) {
            logger.error("druid configuration initialization filter", e)
        }

        return datasource
    }
}
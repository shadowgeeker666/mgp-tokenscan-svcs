package com.mgpchain.tokenscan.config

import com.mgpchain.blockscan.common.until.CallableTaskFrameWork
import com.mgpchain.blockscan.common.until.ConcurrentThreadPool
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/7/16
 * @description：
 * @modified By：
 **/
@Configuration
open class CallableTaskConfig {
    @Bean
    open fun concurrentThreadPool(): ConcurrentThreadPool {
        val concurrentThreadPool = ConcurrentThreadPool()
        val cpuNum = Runtime.getRuntime().availableProcessors()
        concurrentThreadPool.initConcurrentThreadPool(2 * cpuNum)
        return concurrentThreadPool
    }

    @Bean
    open fun callableTaskFrameWork(pool: ConcurrentThreadPool): CallableTaskFrameWork {
        return CallableTaskFrameWork(pool)
    }

}
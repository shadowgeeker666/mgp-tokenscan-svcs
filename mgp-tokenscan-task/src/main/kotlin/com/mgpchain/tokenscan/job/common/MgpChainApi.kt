package com.mgpchain.tokenscan.job.common

import com.alibaba.fastjson.JSONObject
import com.mgpchain.blockscan.common.until.JsonRpcClient
import com.mgpchain.tokenscan.config.Environment
import com.mgpchain.tokenscan.job.BlockScanHandler
import com.mgpchain.tokenscan.job.domain.BlockInfo
import com.mgpchain.tokenscan.job.domain.ChainInfo
import org.slf4j.LoggerFactory

object MgpChainApi {

    fun getBlock(blockNumOrId: String): BlockInfo {
        val url = "${Environment.MGP_NODE}/v1/chain/get_block"
        val client = JsonRpcClient(url, true)
        val request = JSONObject()
        request["block_num_or_id"] = blockNumOrId
        return client.executeJsonPost(request, BlockInfo::class.java)
    }

    fun getInfo(): ChainInfo {
        val url = "${Environment.MGP_NODE}/v1/chain/get_info"
        log.info("【url】--->${url}")
        val client = JsonRpcClient(url, true)
        return client.executeJsonGet(ChainInfo::class.java)
    }

    fun getAccount(accountName: String): JSONObject {
        val url = "${Environment.MGP_NODE}/v1/chain/get_account"
        val client = JsonRpcClient(url, true)
        val request = JSONObject()
        request["account_name"] = accountName
        return client.executeJsonPost(request, JSONObject::class.java)
    }

    fun getTransaction(trxId: String): JSONObject {
        val url = "${Environment.MGP_NODE}/v1/history/get_transaction"
        val client = JsonRpcClient(url, true)
        val request = JSONObject()
        request["id"] = trxId
        return client.executeJsonPost(request, JSONObject::class.java)
    }

    private val log = LoggerFactory.getLogger(BlockScanHandler::class.java)
}
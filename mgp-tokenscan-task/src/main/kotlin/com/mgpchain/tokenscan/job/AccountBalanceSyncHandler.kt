package com.mgpchain.tokenscan.job

import com.mgpchain.blockscan.common.biz.service.MbAccountService
import com.mgpchain.tokenscan.job.common.MgpChainApi
import com.xxl.job.core.context.XxlJobHelper
import com.xxl.job.core.handler.annotation.XxlJob
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class AccountBalanceSyncHandler() {

    @XxlJob("accountBalanceSyncHandler")
    fun accountBalanceSyncHandler() {

        // param parse
        // param parse
        val param = XxlJobHelper.getJobParam()
        if (param == null || param.trim { it <= ' ' }.length == 0) {
            XxlJobHelper.log("param[$param] invalid.")
            XxlJobHelper.handleFail()
            return
        }

        //查询账户表中所有的需要同步余额得账户，计数器减一
        val accounts = mbAccountService.findBySyncFlagGreaterThan(0)
        accounts?.forEach {
            XxlJobHelper.log("sync account ${it.account} balance")
            val account = MgpChainApi.getAccount(it.account)
            try {
                it.unstaked = account["core_liquid_balance"].toString().split(" ")[0].toBigDecimal()

                if (account["voter_info"] != null) {
                    it.staked = account.getJSONObject("voter_info").getBigDecimal("staked").divide(BigDecimal(10_000))
                }
                it.totalBalance = it.staked + it.unstaked
            } catch (e: Exception) {
                XxlJobHelper.log("account ${it.account} sync balance error, cant find balance")
            }
            it.syncFlag = if (it.syncFlag - 1 >= 0) it.syncFlag - 1 else 0
            mbAccountService.save(it)
        }
        return
    }

    @Autowired lateinit var mbAccountService: MbAccountService
}
package com.mgpchain

import com.mgpchain.blockscan.common.biz.env.BaseEnv
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
//@EnableDiscoveryClient
open class Application

fun main(args: Array<String>) {
    BaseEnv.init()
    SpringApplication.run(Application::class.java, *args)
}